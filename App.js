import * as React from 'react';
import Home from './Screens/Home';
import Courses from './Screens/Courses';
import Assignments from './Screens/Assignments';
import Calendar from './Screens/Calendar';
import ToDo from './Screens/ToDo';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';



const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Courses" component={Courses} />
        <Tab.Screen name="Assignments" component={Assignments} />
        <Tab.Screen name="Calendar" component={Calendar} />
        <Tab.Screen name="ToDo" component={ToDo} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}