import React from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const ToDo = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'stretch' }}>
        <Text>{`[value: ${this.state.value0}]`}</Text>
        <CheckBox
          disabled={true}
          value={this.state.value0}
          onValueChange={(value) =>
            this.setState({
              value0: value,
            })
          }
        />
        <Text>{`[value: ${this.state.value1}]`}</Text>
        <CheckBox
          value={this.state.value1}
          tintColors={{true: '#ff0000'}}
          onValueChange={(value) =>
            this.setState({
              value1: value,
            })
          }
        />
        <Button
          onPress={() =>
            this.setState({
              value1: !this.state.value1,
            })
          }
          title="toggle the value above"
        />
      </View>
  );
}

export default ToDo;