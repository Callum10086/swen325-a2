import React from 'react';
import {Alert, Text, Button, View, ScrollView } from 'react-native';

function onPressButton() {  
  Alert.alert('Moving to another page')  
} 

function move() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="A1" component={Assignment1} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const Home = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'stretch' }}>
      <ScrollView >  
        <Text style={{fontSize:40}}>Assignments Due</Text>  
        <Button title={'Assignment1\nSWEN325'} onPress={onPressButton} />  
        <Text style={{fontSize:40}}>Timetable</Text>  
        <Button title={'Lecture\nSWEN325\n10:00-10:50am'} onPress={onPressButton}/>  
        <Button title={'Tutorial\nCOMP102\n12:00-12:50am'} onPress={onPressButton}/>
        <Button title={'Rugby Training\n8:00-9:30pm'} onPress={onPressButton}/>
      </ScrollView> 
    </View>
    
  );
};

export default Home;