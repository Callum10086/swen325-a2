import React from 'react';
import {Alert, Text, Button, View, ScrollView } from 'react-native';

function onPressButton() {  
  Alert.alert('You clicked the button!')  
} 

const Courses = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'stretch' }}>
      <ScrollView >    
        <Button title={'SWEN325'} onPress={onPressButton} />   
        <Button title={'COMP102'} onPress={onPressButton}/>  
        <Button title={'NWEN241'} onPress={onPressButton}/>
        <Button title={'ENGR123'} onPress={onPressButton}/>
      </ScrollView>
    </View>
  );
};
export default Courses;